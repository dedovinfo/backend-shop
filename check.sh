check () {
  pip install -r requirements/linter.txt
  black app/
  black requirements/
  black tests/
  black config.py
  pep257 ./app --ignore=D100,D101,D104,D202,D203
  pep257 ./requirements --ignore=D100,D101,D104,D202,D203
  pep257 ./tests --ignore=D100,D101,D104,D202,D203
  pep257 config.py --ignore=D100,D101,D104,D202,D203
  flake8
}

help () {
  echo "check - проверка"
  echo "Пример запуска ./check.sh check"
  }

case $1 in
"check" ) check;;
* ) help ;;
esac
