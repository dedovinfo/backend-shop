from fastapi import FastAPI
from app.api.routes.routes import router
import uvicorn
from config import DEBUG, PREFIX


def get_application() -> FastAPI:
    """Функция для инициализации приложения FastAPI.

    :return: FastAPI
    """
    application = FastAPI(debug=DEBUG)
    application.include_router(router, prefix=PREFIX)
    return application


app = get_application()

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
