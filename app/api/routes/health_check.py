from fastapi import APIRouter

router = APIRouter()


@router.get("/health_check")
def health_check():
    """Проверочный эндпоинт хелсчек, для проверки жизни сервиса.

    :return:
    """
    return {"service": "OK"}
